from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.urls import reverse


class Post(models.Model):
    options1 = (
        ('Novo', 'Novo'),
        ('Usado', 'Usado'),
    )
    options2 = (
        ('Disponível', 'Disponível'),
        ('Vendido', 'Vendido'),
    )
    

    title = models.CharField(max_length=100)                    # nome do leilão
    value = models.CharField(max_length=100)                    # valor inicial                               
    used = models.CharField(max_length=100, choices=options1)   # indicador se o item leiloado é usado
    sold = models.CharField(max_length=100, choices=options2)   # indicador se o leilão está foi finalizado
    date_open = models.DateTimeField(default=timezone.now())    # data abertura
    date_close = models.DateTimeField(default=timezone.now())   # data finalização
    author = models.ForeignKey(User, on_delete=models.CASCADE)  # usuário responsável pelo leilão | deleta user -> deleta posts (nao contrario)

    def __str__(self):
        return self.title
    
    # Pega uma instância específica de um post
    def get_absolute_url(self):
        return reverse('post-detail', kwargs={'pk': self.pk})
