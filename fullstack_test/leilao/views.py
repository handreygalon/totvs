from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.db import models
from django.utils import timezone
from datetime import datetime
from .models import Post
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView
)


@login_required
def home(request):
    # context = {'posts': posts}
    context = {
        'posts': Post.objects.all()
    }

    return render(request, 'leilao/home.html', context)

class PostListView(ListView):
    model = Post
    template_name = 'leilao/home.html'  # <app>/<model>_<viewtype>.html
    context_object_name = 'posts'
    ordering = ['-date_open']


class PostDetailView(DetailView):
    model = Post


class PostCreateView(LoginRequiredMixin, CreateView):
    model = Post
    fields = ['title', 'value', 'used', 'sold']

    # define o usuario criador da postagem
    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class PostUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Post
    fields = ['title', 'value', 'used', 'sold']

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)
    
    # Verifica o usuario antes de permitir atualizar a postagem
    def test_func(self):
        post = self.get_object()  # Recupera a postagem que será atualizada

        if post.sold == 'Vendido':
            post.date_close = datetime.now()
            post.save(update_fields=['date_close'])

        if self.request.user == post.author:  # Recupera o usuario logado
            return True
        return False


class PostDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Post
    success_url = '/'  # Retorna para inicio

    def test_func(self):
        post = self.get_object()

        if self.request.user == post.author:
            return True
        return False