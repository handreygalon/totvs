# Generated by Django 2.2.3 on 2019-07-03 03:21

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('leilao', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='date_close',
            field=models.DateTimeField(default=datetime.datetime(2019, 7, 3, 3, 21, 35, 440646, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='post',
            name='date_open',
            field=models.DateTimeField(default=datetime.datetime(2019, 7, 3, 3, 21, 35, 440646, tzinfo=utc)),
        ),
    ]
