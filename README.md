1. Instalar Python versão 3.7.3 -> https://www.python.org/downloads/

2. Inslatar pip:  

   - Realizar o download do arquivo get-pip.py em <https://bootstrap.pypa.io/get-pip.py>  
   
   - Executar via prompt de comando <python get-pip.py>  
   
   - <pip --version> -> 19.1.1  
   

3. Instalar ambiente de desenvolvimento:  

   - <pip install virtualenv>  
   
   - <pip install virtualenvwrapper-win>  
   
   - <virtualenv --version> -> 16.6.1  
   

4. Criar diretório para o ambiente de desenvolvimento:  

   - <mkvirtualenv venv>  
   
   - Acessar o diretório do ambiente: <cd Envs/venv>  
   
   - Ativar ambiente de desenvolvimento <.\Scripts\activate>  
   
   - Desativar ambiente de desenvolvimento <deactivate> (Somente após encerrar aplicação)  
   

5. Inslatar django   

   - <pip install django>  
   
   - <django-admin version> -> 2.2.3  
   
   - <pip install django-crispy-forms>  
   
 
6. Copiar a pasta "fullstack_test" para dentro do ambiente de desenvolvimento criado <Envs/venv>  


7. Acessar a pasta "fullstack_test" <cd fullstack_test>  


8. Rodar servidor: <python manage.py runserver>  
